# Install Helm 
 ## From Script:

    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    chmod 700 get_helm.sh
    ./get_helm.sh 

 ## From apt:
    
    curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
    sudo apt-get install apt-transport-https --yes
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
    sudo apt-get update
    sudo apt-get install helm  

# Install IngressController with Helm
  ## Script to install:

    helm repo add nginx-stable https://helm.nginx.com/stable
    helm repo update
    helm install nginx-ingress nginx-stable/nginx-ingress --set rbac.create=true

  ## Check pod and service:

     kubectl get pods
     kubectl get svc

# Install NFS Server
 ## On Server:
    sudo apt update
    sudo apt install nfs-kernel-server

  ### Create the share directory:
    
    mkdir -p /mnt/nfs-dir 
  
  ### change the directory ownership:
  
    sudo chown nobody:nogroup /mnt/nfs-dir
  
  ### Configure the NFS exports file :

    sudo vim /etc/exports
    /mnt/nfs-dir client_ip(rw,sync) 
    .others options you could use (no_subtree_check,no_root_squash)
  ### Restart nfs server:

    sudo systemctl restart nfs-kernel-server      
 
 ## On Client:

    sudo apt update
    sudo apt install nfs-common    

  ### Create the directory for mount:

    mkdir -p /home/nfs-mount

  ### Mount the shared directory:

    sudo mount nfs-server_address_ip:/mnt/nfs-dir /home/nfs-mount
  ### check if the directory mounted successfully:

    df -h    
# Integrating NFS Server with Kubernetes Cluster   
 ## Install Helm Repo for nfs-subdir-external-provisioner:

    helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner   
 ## Install helm Chart for NFS:

    helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --set nfs.server={nfs_server_address_ip} --set nfs.path=/mnt/nfs-dir --set storageClass.onDelete=true  
 ## Create Persistent Volume Claim (PVC)

    apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
        name: nfs-pvc
    spec:
        accessModes:
         - ReadWriteOnce
        storageClassName: nfs-client
    resources:
        requests:
          storage: 3Gi   

     