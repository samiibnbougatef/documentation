An ideal mind map for running #production ready #Kubernetes platform ☸
Setting up a resilient, stable & operational #k8s platform involves crafting important #platform #metrics that makes the life of a #developer a lot easier and gives them time to focus on delivering business value

✅ 𝐈𝐧𝐟𝐫𝐚𝐬𝐭𝐫𝐮𝐜𝐭𝐮𝐫𝐞 𝐚𝐬 𝐂𝐨𝐝𝐞 (𝐈𝐚𝐂): Managing #cloud infrastructure with #IaC discourages or prevents manual deployments, making #infrastructure deployments more consistent, reliable and repeatable. Tools like #Terraform or #Pulumi work great to deploy entire Kubernetes cluster in any cloud together with networking, load balancers, DNS configuration and of course an integrated Container Registry

✅ 𝐌𝐨𝐧𝐢𝐭𝐨𝐫𝐢𝐧𝐠 & 𝐂𝐞𝐧𝐭𝐫𝐚𝐥𝐢𝐳𝐞𝐝 𝐥𝐨𝐠𝐠𝐢𝐧𝐠:
 Right set of #Observability tools, processes and practises is important for centralized #logging, #monitoring, #tracing #eventing for all the events in the deployed cluster. #Prometheus and #Grafana works great for #monitoring events on platform while #Filebeat or #Logstash can be good #oss alternative for Logging

✅ 𝐂𝐞𝐧𝐭𝐫𝐚𝐥𝐢𝐳𝐞𝐝 𝐈𝐧𝐠𝐫𝐞𝐬𝐬 𝐂𝐨𝐧𝐭𝐫𝐨𝐥𝐥𝐞𝐫 𝐰𝐢𝐭𝐡 𝐒𝐒𝐋 𝐜𝐞𝐫𝐭𝐢𝐟𝐢𝐜𝐚𝐭𝐞 𝐦𝐚𝐧𝐚𝐠𝐞𝐦𝐞𝐧𝐭: 
Incoming traffic to the #pods with a valid HTTPs certificate issued by an internal CA or external approved CA. Cert-manager centrally deployed application in Kubernetes that takes care of HTTPS certificates and can be configured using Let's Encrypt, wildcard certificates or even a private Certification Authority for internal company-trusted certificates

✅ 𝐑𝐨𝐥𝐞-𝐁𝐚𝐬𝐞𝐝 𝐀𝐜𝐜𝐞𝐬𝐬 𝐂𝐨𝐧𝐭𝐫𝐨𝐥: Applying the principle of least privilege via #RBAC policies to the API , dashboards and deployments. Can be further complimented using external solutions to manage authentication and authorization using #OAuth2 / #OIDC for both #platform tools and applications

✅ 𝐆𝐢𝐭𝐎𝐩𝐬 𝐛𝐚𝐬𝐞𝐝 𝐃𝐞𝐩𝐥𝐨𝐲𝐦𝐞𝐧𝐭𝐬: Implementing #GitOps principles to manage #k8s desired state configuration for applications and configurations instead of doing using 'kubectl apply' way so that changes are 100% traceable, easily automated, manageable & error-free. #Argocd, #Flux are perfect candidates that handles real-time declarative state management, making sure that #Git is the single source of truth for the Kubernetes state 

✅ 𝐒𝐞𝐜𝐫𝐞𝐭 𝐌𝐚𝐧𝐚𝐠𝐞𝐦𝐞𝐧𝐭 𝐬𝐭𝐫𝐚𝐭𝐞𝐠𝐲: Helps to manage environment variables or configurations traditionally outside the application scope and provides an option to lock out all developers from secrets in Kubernetes using RBAC. Tools such as #hashicorp #Vault, #Bitnami Sealed Secrets or #AWS Secrets Manager with a central secrets operator like External Secrets Operator helps to sync secrets on #k8s platform

✅ 𝐒𝐞𝐫𝐯𝐢𝐜𝐞 𝐌𝐞𝐬𝐡: Helps to navigate the routing between applications while defining a dedicate control plane for managing data traffic enforcing security, mTLS, encryption on the E2E traffic as well

![Alt text](https://media.licdn.com/dms/image/C5622AQEeK2ATpist9g/feedshare-shrink_800/0/1679031449755?e=1681948800&v=beta&t=o4WIqcRAvSaYwIOyFBU5XOHFxQNQyCjwX4rs_0CB3Dk)