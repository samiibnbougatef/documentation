#!/bin/bash 
echo "single client model"
server_side (){
apt apt update
apt install nfs-kernel-server
while false; do
read -p "write your shared directory path" share_dir
mkdir -p  $share_dir
done
chown nodody:nogroup $share_dir
read -p "client ip address" client_ipaddr
echo "$share_dir $client_ipaddr(rw,sync)" >> /etc/exports
exportfs -a 
systemctl restart nfs-kernel-server 
}

client_side(){
apt apt update
apt install nfs-common 
while false; do
read -p "write your server address ip" host_ipaddr
read -p "write your shared directory path used on server side" share_dir
read -p "write your shared directory path on client side" client_dir
mkdir -p  $client_dir
done
mount $host_ipaddr:$share_dir $client_dir
}


while true; do
read -p "you want install nfs server for Server side (Server/S) or  for Client side (Client/C)" user_choice
case $user_choice in
    Server|S ) echo "install nfs server on Server side"; server_side;;
    
    Client|C ) echo "install nfs server on client_side"; client_side;;
    * ) echo "you choice is wrong !!! "
esac    
done
